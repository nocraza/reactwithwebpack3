import React from 'react'
import ReactDOM from 'react-dom'

import { Provider } from "react-redux";
import Store from "./store";

import App from './components/App'

import Firebase, {FirebaseContext} from "./components/firebase";

import { activeTabChanged } from "./actions";

import './index.scss'

import 'materialize-css/dist/js/materialize'
import '@fortawesome/fontawesome-free/js/all'

// console.log(activeTabChanged({activeTab: 'Ariel Pogi'}))

ReactDOM.render(
  <Provider store = {Store}>
    <FirebaseContext.Provider value = {new Firebase()} >
      <App />
    </FirebaseContext.Provider>
  </Provider>,
  document.getElementById('app')
)

