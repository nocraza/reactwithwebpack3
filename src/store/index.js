import { activeTabReducer } from "./../reducers";
import { createStore } from "redux";

const INITIAL_STATE = {
  author: "Ariel Pogi",
  navActiveTab: "Sign up"
}

export default createStore( activeTabReducer,INITIAL_STATE, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() )