const navbarActiveTab = 'CHANGE_ACTIVE_TAB'

const userSignIn = 'USER_SIGNED_IN'



export const activeTabChanged = (activeTab) => {
  type: navbarActiveTab,
  activeTab
}

export const userSignedIn = loggedInUser => {
  return {
    type: userSignIn,
    loggedInUser
  }
}

