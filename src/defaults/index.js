export const materializeToast = {
  classes: 'teal lighten-2',
  displayLength: 3000
}