import React, { Component } from 'react'
import { withFirebase } from "./../firebase";
import './SignUpForm.scss'

import { materializeToast } from "./../../defaults";

class SignUpForm extends Component {
  constructor(props){
    super(props)

    this.state = {
      email: '',
      password: '',
      confirmpassword: '',
      name: '',
      error: null
    }

  }

  componentDidMount(){
  }

  onSubmit = e => {
    const {email, password, name} = this.state
    e.preventDefault()
    const M = window.M  //materialize

    // console.log(this.state)
    this.props.firebase
      .signUp_Email_Password(email, password)
      .then(auth => {
        M.toast({
          html:"Successfully signed up! <br/> Now you can sign in",
          ...materializeToast
        })
        this.props.firebase.user(auth.user.uid)
          .set({
            email,
            name
          })
        this.props.history.push('/signin')
      })
      .catch(error => {
        M.toast({
          html: error.message,
          ...materializeToast
        })
        console.clear()
      })

  }

  onChange = e => this.setState({
    [e.target.name]:e.target.value
  })

  render() {
    const {error, email, password, confirmpassword, name} = this.state

    const isInvalid = password !== confirmpassword || password === '' || email === '' || name === ''
    return (
      <form onSubmit = {this.onSubmit}>

        <div className='signup card'>
          <h3 className = 'signup-title'>Sign Up</h3>
          <div className="input-field">
            <label htmlFor = 'signup-email'>
              Email
            </label>
            <input 
                type="email" 
                name="email" 
                id="signup-email"
                onChange = {this.onChange}
                />
          </div>

          <div className="input-field">
            <label htmlFor = "signup-name">
              Name
            </label>

            <input 
                type="text" 
                name="name" 
                id="signup-name"
                onChange = {this.onChange}
                />
          </div>

          <div className="input-field">
            <label htmlFor = "signup-password">
              Password
            </label>

            <input 
                type="password" 
                name="password" 
                id="signup-password"
                onChange = {this.onChange}
                />
          </div>
          

          <div className="input-field">
            <label htmlFor = "signup-password">
              Confirm Password
            </label>

            <input 
                type="password" 
                name="confirmpassword" 
                id="signup-confirm-password"
                onChange = {this.onChange}
                />
          </div>

          <button
            type = "submit"
            className = 'btn teal waves-effect waves-light'
            disabled = {isInvalid}
            >
            Sign Up
          </button>

        </div>
      
      {error && <p> {error.message} </p> }
      </form>
    )
  }
}

export default withFirebase(SignUpForm)
