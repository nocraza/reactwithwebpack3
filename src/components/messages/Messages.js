import React, { PureComponent } from 'react'

import { connect } from "react-redux";

import { Redirect } from "react-router-dom";

import './Messages.scss'

class Messages extends PureComponent {
  constructor(props){
    super(props)

  }

  componentDidMount(){
    // if(!this.props.loggedInUser){this.props.history.push('/signin')}
  }
  render() {
    if(!this.props.loggedInUser)return <Redirect to="/signin" />
    return (
      <div>
        <h1>Messages</h1>
        <div className = "messages-collection-container">

          <ul className="collection">
            <li className="collection-item">Alvin</li>
            <li className="collection-item">Alvin</li>
            <li className="collection-item">Alvin</li>
            <li className="collection-item">Alvin</li>
          </ul>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({loggedInUser: state.loggedInUser})

export default connect(mapStateToProps)(Messages)
