import app from "firebase/app";

import 'firebase/auth'

import 'firebase/database'

const firebaseConfig = {
  apiKey: "AIzaSyC7LSX5J1zWFW6ruyKpNOiTp9w_R_mjOvw",
  authDomain: "testingfirestore-eadad.firebaseapp.com",
  databaseURL: "https://testingfirestore-eadad.firebaseio.com",
  projectId: "testingfirestore-eadad",
  storageBucket: "testingfirestore-eadad.appspot.com",
  messagingSenderId: "865223485259",
  appId: "1:865223485259:web:cf04c10122b412da"
};

class Firebase{
  constructor(){
    app.initializeApp(firebaseConfig)

    this.db = app.database()
    this.auth = app.auth()

    // console.log(this.db.ref('users'))
    let users = []
    const f = this.db.ref('users')
    f.on("value", function(snapshot) {
      // Object.keys(snapshot.val()).forEach((k, i) => {
      //   const { email, username } = snapshot.val()[k]
      //   email === 'ariel@ctoglobal.co' && console.log(username)
      // })
      // console.log(Object.keys(snapshot.val()).length)
      // console.log(snapshot.val())
    }, function (errorObject) {
      console.log("The read failed: " + errorObject.code);
    });

    const ttt = this.db.ref('tictactoe')
    ttt.on("value", function(snapshot) {
      // console.log(Object.keys(snapshot.val()).length)
      snapshot.val() && console.log(snapshot.val())
    }, function (errorObject) {
      console.log("The read failed: " + errorObject.code);
    });
  }

  users = () => this.db.ref('users')

  user = uid => this.db.ref(`users/${uid}`)

  signUp_Email_Password = (email, pass) => this.auth.createUserWithEmailAndPassword(email, pass)
  signIn_Email_Password = (email, pass) => this.auth.signInWithEmailAndPassword(email, pass)
  signOut = () => this.auth.signOut()


  getName = uid => {
    let name = ''
    this.user(uid)
      .on("value", snapshot => {
        name = snapshot.val().name
      })
      return name
  }

  // tictactoe = () => this.db.ref('tictactoe')
  // tttmove = number => this.db.ref()

  // sample = this.db.ref('users')

  
}

export default Firebase
