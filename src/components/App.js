import React, {Component} from 'react'

import Navbar from './navbar/Navbar'
import { BrowserRouter, Route } from 'react-router-dom';

import SignInForm from './signin'
import SignUpForm from './signup'

import Messages from './messages'

import Tictactoe from "./tictactoe";

import "./App.scss"


class App extends Component {
  render(){
    return (
      <BrowserRouter>
        <Navbar />


        <Route 
          exact
          path = '/signin'
          component={SignInForm}
        />

        <Route 
          exact
          path = '/'
          component={Tictactoe}
        />

        <Route 
          path = '/signup'
          component={SignUpForm}
        />

        <Route 
          path = '/messages'
          component={Messages}
        />
        {/* <div className = "tempContainer">

          <Tictactoe />
        </div> */}
      </BrowserRouter>
    )
  }
}

export default App