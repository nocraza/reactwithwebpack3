import React, { useState } from 'react'
import { Link, withRouter } from "react-router-dom";

import { ReactReduxContext, connect } from "react-redux";

import { withFirebase } from "./../firebase";

import { materializeToast } from "./../../defaults";

import './Navbar.scss'

const mapStateToProps = state => ({
  // author: state.author, 
  // loggedInUser: state.loggedInUser,
  // history: state.history
  ...state
})

const Navbar = (props) => {

  const onClick = e =>{
    e.preventDefault()
    // console.log(props);
    props.firebase
      .signOut()
      .then(()=> {
        window.M.toast({
          html: "Successfully logged out!",
          ...materializeToast
        })
        props.history.push('/signin')
      })
  }

  const NavbarNonAuth = () => (
    <nav>
      <div className="nav-wrapper">
        <Link to="/" className="brand-logo"><i className="fas fa-hand-middle-finger"></i>UMessage</Link>
        <ul id="nav-mobile" className="right hide-on-med-and-down">
          <li><Link to="/signin">Sign In</Link></li>
          <li><Link to="/signup">Sign Up</Link></li>
        </ul>
      </div>
    </nav>
  )

  const NavbarAuth = () => (
    <nav>
      <div className="nav-wrapper">
        <Link to="/" className="brand-logo"><i className="fas fa-hand-middle-finger"></i>UMessage</Link>
        <ul id="nav-mobile" className="right hide-on-med-and-down">
          <li><Link to="#">{props.loggedInUser.name}</Link></li>
          <li><Link to="/messages">Messages</Link></li>
          <li><Link to="#" onClick={onClick}>Logout</Link></li>
        </ul>
      </div>
    </nav>
  )

  

  return (
    // <ReactReduxContext.Consumer>
    //   {({store}) => {<nav>
    //     <div className="nav-wrapper">
    //       <Link to="/" className="brand-logo"><i className="fas fa-hand-middle-finger"></i>UMessage</Link>
    //       <ul id="nav-mobile" className="right hide-on-med-and-down">
    //         <li> {store.author} </li>
    //         <li><Link to="/signin">Sign In</Link></li>
    //         <li><Link to="/signup">Sign Up</Link></li>
    //       </ul>
    //     </div>
    //   </nav>}}  
    // </ReactReduxContext.Consumer> 
    
    // idk how the above works
    <React.Fragment>
      {props.loggedInUser ? <NavbarAuth /> : <NavbarNonAuth />}
    </React.Fragment>


    
    
  )
}


export default withRouter(withFirebase(connect(mapStateToProps)(Navbar)))

