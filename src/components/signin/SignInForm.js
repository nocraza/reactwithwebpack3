import React, { Component } from 'react'

import { connect } from "react-redux";

import { userSignedIn } from "./../../actions";

import { withFirebase } from "./../firebase";

import { materializeToast } from "./../../defaults";

import './SignInForm.scss'

class SignInForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      email: '',
      password: '',
      name: ''
    }
  }

  componentDidMount(){
    // console.log(this.props)
    // console.log('Signin ComponentDidmount ', this.props)
  }

  componentWillReceiveProps (nextProps) {
    // console.log(nextProps)
  }


  onSubmit = e =>{
    const {email, password} = this.state;
    const {firebase} = this.props
    e.preventDefault();
    // console.log('onSubmit ', this.state)
    // dispatch(this.state)
    

    // console.log(userSignedIn(this.state))
    // this.props.asd('ariel pogi edited')

    firebase.signIn_Email_Password(email, password)
      .then(auth => {
        // console.log('SignInForm onSubmit ', auth.user)
        this.setState({name: firebase.getName(auth.user.uid)})
        this.props.usi(this.state)

        this.props.history.push('/messages')
      })
      .catch(e => {
        window.M.toast({
          html:e.message,
          ...materializeToast,
          classes: "red"
        })
        console.clear()
      })

  }

  onChange = e =>{
    e.preventDefault();
    // console.log(e.target.value)
    this.setState({
      [e.target.name]: e.target.value
    })

    // this.setState({})
  }

  render() {
    // console.log('render ', this.props)
    return (
      <form
        onSubmit = {this.onSubmit}
      >

        <div className='signin card'>
          <h3 className = 'signin-title'>Sign In</h3>
          <div className="input-field">
            <label htmlFor = 'signin-username'>
              Username
            </label>
            <input 
              type="text" 
              name="email" 
              id="signin-username"
              onChange = {this.onChange}
            />
          </div>

          <div className="input-field">
            <label htmlFor = "signin-password">
              Password
            </label>

            <input 
                type="password" 
                name="password" 
                id="signin-password"
                onChange = {this.onChange}
                />
          </div>

          <button
            type = "submit"
            className = 'btn teal waves-effect waves-light'
          >
            Sign In
          </button>

        </div>
      </form>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  usi: user => dispatch(userSignedIn(user)),
  asd: author => dispatch({type: 'CHANGE_AUTHOR',author})

})

const mapStatetoTrops = state => ({...state})

export default withFirebase(connect(mapStatetoTrops, mapDispatchToProps)(SignInForm))