import React, {useState} from 'react'

import "./Tictactoe.scss"

const tictactoe = () => {
  return (
    <div className = "tttContainer">
      <div className = 'tictactoe'>
        <Square number = {1} />
        <Square number = {2} />
        <Square number = {3} />
        <Square number = {4} />
        <Square number = {5} />
        <Square number = {6} />
        <Square number = {7} />
        <Square number = {8} />
        <Square number = {9} />
      </div>
    </div>
  )
}

let last = '0';

const Square = ({number}) => {
  const [testHook, settestHook] = useState('')
  const [eventOnce, setEventOnce] = useState(false)

  const asdf = (number, e) => {
    last = last == '0' ? 'X': '0'
    // console.log(e.target.parentNode)
    
    settestHook(last)
    // console.log(e.target.remove)
    e.target.diabled = true

    setEventOnce(true)
  }
  return (
    <div
      className = {" tictactoe-square " + (eventOnce ? 'disable-events' : '') }
      // onClick = {() => settestHook('X')}
      onClick = {(e) => asdf(number, e)}
    >
      {testHook}
    </div>
  )

  

  // function asdf() {
  //   settestHook('X')
  // }

  
  // const asdf = () => settestHook("A")
  
}

export default tictactoe
