export const activeTabReducer = (state, action) => {
  switch(action.type) {
    case 'CHANGE_ACTIVE_TAB':
      return {
        ...state,
        navbarActiveTab: action.activeTab
      }
    case 'USER_SIGNED_IN':
      return {
        ...state,
        loggedInUser: action.loggedInUser
        // loggedInUser
      }
    case 'CHANGE_AUTHOR': 
      return {
        ...state,
        author: action.author
      }
    default:
      return state
  }
}